import socket
from AES import *
from random import randrange
import pickle

# create a socket object
proxysocket = socket.socket(
    socket.AF_INET, socket.SOCK_STREAM)

# get local machine name
host = socket.gethostname()

port_proxy = 501
keyword = "Network_security"

# bind to the port
proxysocket.bind((host, port_proxy))

# queue up to 5 requests
proxysocket.listen(5)

print("Sono il proxy e attendo connessione del client")

while True:
    # establish a connection
    clientsocket, addr = proxysocket.accept()

    msg = clientsocket.recv(1024)
    clientsocket.send(b"Proxy: Messaggio criptato ricevuto!")
    msg3 = clientsocket.recv(1024)
    cripto = Crypto(keyword)
    msg4 = cripto.decrypt(msg3)
    msg5 = pickle.loads(msg4) # pickle: binary protocols for serializing and de-serializing a Python object structure
    msg5.remove(port_proxy)
    if (len(msg5) != 1):
        port = msg5[randrange(1, len(msg5))]
    else:
        port = msg5[0]
        print("Mando messaggio segreto al server finale")

    data_array = pickle.dumps(msg5)
    clientsocket.send(b"Proxy: Lista criptata delle porte ricevuta!")
    clientsocket.close()

    # bind to the port
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    # get local machine name
    host = socket.gethostname()
    # connection to hostname on the port.
    s.connect((host, port))
    s.send(msg);
    print(s.recv(1024).decode("UTF-8"))
    cripto = Crypto(keyword)
    msg2 = cripto.encrypt(data_array)
    s.send(msg2)
    print(s.recv(1024).decode("UTF-8"))
    s.close()