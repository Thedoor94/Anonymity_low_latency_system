import socket
import pickle
from AES import *
from Cryptodome.Cipher import PKCS1_OAEP
from Cryptodome.PublicKey import RSA
from random import randrange

# create a socket object
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# get local machine name
host = socket.gethostname()
server_port = 800

secret_msg = "I met UFO at UNIPR".encode("utf-8")

recipient_key = RSA.import_key(open("Public_key_server.pem").read())
cipher_rsa = PKCS1_OAEP.new(recipient_key)
cipher_text = cipher_rsa.encrypt(secret_msg)

port_list = [server_port, 500, 501, 502]  # lista delle porte della rete fidata di proxy
keyword = "Network_security" # password comune fra client e rete di proxy per crittografia simmetrica AES
port = port_list[randrange(1, len(port_list))]
data_array = pickle.dumps(port_list)
# connection to hostname on the port.
s.connect((host, port))

print("Sono il client e mando il messaggio criptato ad un proxy casuale")
s.send(cipher_text)
print(s.recv(1024).decode("UTF-8"))
cripto = Crypto(keyword)
msg = cripto.encrypt(data_array)
s.send(msg)
print(s.recv(1024).decode("UTF-8"))
s.close()
