import socket
from KeyThread import *
from Cryptodome.PublicKey import RSA
from Cryptodome.Cipher import PKCS1_OAEP

key = RSA.generate(2048)
thread_key = ThreadKey("ThreadPublicKey", key);
thread_key.start()

private_key = key.export_key()

# create a socket object
serversocket = socket.socket(
    socket.AF_INET, socket.SOCK_STREAM)

# get local machine name
host = socket.gethostname()

port = 800
keyword = "password"

# bind to the port
serversocket.bind((host, port))

# queue up to 5 requests
serversocket.listen(1)

print("Sono il server e sto aspettando una connessione\n")

while True:
    # establish a connection
    clientsocket, addr = serversocket.accept()

    msg = clientsocket.recv(1024)
    cipher_rsa = PKCS1_OAEP.new(key)
    msg2 = cipher_rsa.decrypt(msg)
    print("Ho ricevuto il messaggio segreto dal client\nLo decodifico con la mia chiave privata\n")
    print("Messaggio segreto: " + msg2.decode("UTF-8"))
    clientsocket.send(b"Server: Messaggio segreto criptato ricevuto!")
    msg3 = clientsocket.recv(1024)
    clientsocket.send(b"Server: Fine comunicazione")

    clientsocket.close()
