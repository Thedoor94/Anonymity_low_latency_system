from threading import Thread
from Cryptodome.PublicKey import RSA


class ThreadKey(Thread):
    def __init__(self, nome, key):
        Thread.__init__(self)
        self.nome = nome
        self.key = key

    def run(self):
        print("" + self.nome + " avviato\n")
        public_key = self.key.publickey().export_key()
        file_out = open("Public_key_server.pem", "wb")
        file_out.write(public_key)
        print("Chiave pubblica generata e salvata su file\n")
        print("" + self.nome + " terminato\n")
